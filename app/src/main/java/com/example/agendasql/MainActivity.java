package com.example.agendasql;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import database.AgendaContactos;
import database.Contacto;

public class MainActivity extends AppCompatActivity {
    private Button btnAgregar;
    private TextView lblNombre;
    private AgendaContactos db;
    private EditText edtNombre;
    private EditText edtTelefono;
    private EditText edtTelefono2;
    private EditText edtDireccion;
    private EditText edtNotas;
    private CheckBox cbxFavorito;
    private Contacto savedContact;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnAgregar = (Button) findViewById(R.id.btnGuardar);
        lblNombre = (TextView) findViewById(R.id.lblNombre);
        db = new AgendaContactos(MainActivity.this);
        edtNombre = (EditText) findViewById(R.id.edtNombre);
        edtTelefono = (EditText) findViewById(R.id.edtTel1);
        edtTelefono2 = (EditText) findViewById(R.id.edtTel2);
        edtDireccion = (EditText) findViewById(R.id.edtDomicilio);
        edtNotas = (EditText) findViewById(R.id.edtNota);
        cbxFavorito = (CheckBox) findViewById(R.id.chkFavorito);

        Button btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        Button btnListar = (Button) findViewById(R.id.btnListar);
        Button btnCerrar = (Button) findViewById(R.id.btnCerrar);

        db = new AgendaContactos(MainActivity.this);

        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(edtNombre.getText().toString().matches("") || edtTelefono.getText().toString().matches("") ||
                        edtTelefono2.getText().toString().matches("") || edtDireccion.getText().toString().matches("")) {
                    Toast.makeText(MainActivity.this, "Ingrese todos los datos", Toast.LENGTH_SHORT).show();
                } else {
                    Contacto nContacto = new Contacto();

                    nContacto.setNombre(edtNombre.getText().toString());
                    nContacto.setDomicilio(edtDireccion.getText().toString());
                    nContacto.setNotas(edtNotas.getText().toString());
                    nContacto.setTelefono1(edtTelefono.getText().toString());
                    nContacto.setTelefono2(edtTelefono2.getText().toString());
                    nContacto.setFavorito(cbxFavorito.isChecked() ? 1:0);

                    db.openDataBase();

                    if(savedContact != null){
                        long idx = db.UpdateContacto(nContacto, savedContact.get_ID());
                        Toast.makeText(MainActivity.this, "El contacto se actualizó con éxito: "+ idx, Toast.LENGTH_SHORT).show();
                        savedContact = null;
                    }else{
                        long idx = db.insertarContacto(nContacto);
                        Toast.makeText(MainActivity.this, "El contacto se agregó con éxito: "+ idx, Toast.LENGTH_SHORT).show();
                    }

                    db.cerrarDataBase();
                    limpiar();

                }
            }
        });


        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ListActivity.class);
                startActivityForResult(i,0);

            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (Activity.RESULT_OK == resultCode) {
            Contacto contacto = (Contacto) intent.getSerializableExtra("contacto");
            savedContact = contacto;
//            id = contacto.get_ID();
            edtNombre.setText(contacto.getNombre());
            edtTelefono.setText(contacto.getTelefono1());
            edtTelefono2.setText(contacto.getTelefono2());
            edtDireccion.setText(contacto.getDomicilio());
            edtNotas.setText(contacto.getNotas());
            if(contacto.getFavorito() >0) {
                cbxFavorito.setChecked(true);
            }
        } else {
            limpiar();
        }


    }

    public void limpiar() {
        edtNombre.setText("");
        edtDireccion.setText("");
        edtTelefono.setText("");
        edtTelefono2.setText("");
        edtNotas.setText("");
        cbxFavorito.setChecked(false);
        savedContact = null;
    }
}